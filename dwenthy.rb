# Hoje a gente vai avançar no nosso jogo
# Vamos conhecer seu maior desafio - O Array de Dwemthy
# Mas antes abra a versão 1
# tudo assim, muito parecido
# em programação, repetir é mal
# como a gente pode mudar isso?

class Dragao < Criatura
  def initialize
    @vida = 1340     # escamas duras
    @forca = 451     # veias ressaltadas
    @carisma = 1020  # sorriso dentado
    @arma = 939      # cospe fogo
  end
end

# e que tal...

class Dragao < Criatura
  vida 1340     # escamas duras
  forca 451     # veias ressaltadas
  carisma 1020  # sorriso dentado
  arma 939      # cospe fogo
end

# melhor, não?
# nós vamos alcançar isso com metaprogramação
# código que escreve código

# salve esse código num arquivo 'dwemthy.rb'

# As vísceras da força vital do Array de Dwemthy
class Criatura
  # Obtém uma metaclasse para esta classe
  def self.metaclass; class << self; self; end; end

  # Avançado código de metaprogramação para boas
  # e claras características
  def self.caracteristicas( *arr )
    return @caracteristicas if arr.empty?

    # 1. Define 'accessors' para cada variável
    attr_accessor *arr

    # 2. Adiciona um novo método para cada característica.
    arr.each do |a|
      metaclass.instance_eval do
        define_method( a ) do |val|
          @caracteristicas ||= {}
          @caracteristicas[a] = val
        end
      end
    end

    # 3. Para cada monstro o método 'initialize'
    #    deve usar um número padrão para cada característica.
    class_eval do
      define_method( :initialize ) do
        self.class.caracteristicas.each do |k,v|
          instance_variable_set("@#{k}", v)
        end
      end
    end

  end

  # Os atributos da Criatura são somente leitura
  caracteristicas :vida, :forca, :carisma, :arma
end

# vamos dissecá-lo
# a mágica é o método características

# A lista de características é passada para attr_accessor,
# que constrói código leitor e escritor para as variáveis de instância.
# Um para cada característica.
# Métodos de classe são adicionados para cada característica.
# (O método vida é adicionado para a característica :vida).
# Esses métodos de classe são usados na definição da classe assim como você
# usaria caracteristicas ou @attr_acessor.
# Dessa maneira, você pode especificar a característica, de acordo com os pontos
#  dados de uma característica para uma determinada criatura.
# Adicione um método de inicialização que cria um novo monstro corretamente, com os
# pontos certos e GANHANDO FORÇA! GANHANDO FORÇA! o monstro está vivo!
# é como se tivéssemos escrito

class Criatura

  # 1. define métodos leitores e escritores
  attr_accessor :vida, :forca, :carisma, :arma

  # 2. adiciona novos métodos de classe para usar na criatura
  def self.vida( val )
    @caracteristicas ||= {}
    @caracteristicas['vida'] = val
  end

  def self.forca( val )
    @caracteristicas ||= {}
    @caracteristicas['forca'] = val
  end

  def self.carisma( val )
    @caracteristicas ||= {}
    @caracteristicas['carisma'] = val
  end

  def self.arma( val )
    @caracteristicas ||= {}
    @caracteristicas['arma'] = val
  end

  # 3. estabelece pontos padrões para
  #    cada característica
  def initialize
    self.class.caracteristicas.each do |k,v|
      instance_variable_set("@#{k}", v)
    end
  end

end

## Eval, o menor metaprogramador ##

drgn = Dragao.new
# é identifo a...
drgn = eval( "Dragao.new" )
# ou alternativamente...
eval( "drgn = Dragao.new" )

print "Que classe de monstro você veio combater? "
classe_monstro = gets
eval( "monstro = " + classe_monstro + ".new" )
p monstro

# instance_eval e class_eval
# são como eval, mas mergulham num objeto e executam o código lá dentro

# O método instance_eval executa o código como se ele fosse executado dentro
# do objeto do método de instância.
irb> drgn = Dragao.new
irb> drgn.instance_eval do
irb>   @nome = "Tobias"
irb> end

irb> drgn.instance_variable_get( "@nome" )
=> "Tobias"

# O método class_eval executa o código se estiver dentro da definição de classe.
irb> Dragao.class_eval do
irb>   def nome; @nome; end
irb> end

irb> drgn.nome
=> "Tobias"

# Agora adicione esse trecho ao arquivo:

class Criatura

   # Este método aplica um golpe recebido durante uma luta.
   def golpear( dano )
     aumento_poder = rand( carisma )
     if aumento_poder % 9 == 7
       @vida += aumento_poder / 4
       puts "[Aumento de poderes mágicos de #{ self.class } #{ aumento_poder }!]"
     end
     @vida -= dano
     puts "[#{ self.class } está morto.]" if @vida <= 0
   end

   # Este método obtém uma rodada em uma luta.
   def lutar( inimigo, arma )
     if vida <= 0
       puts "[#{ self.class } está muito morto para lutar!]"
       return
     end

     # Ataca o oponente
     seu_golpe = rand( forca + arma )
     puts "[Você golpeia com #{ seu_golpe } pontos de dano!]"
     inimigo.golpear( seu_golpe )

     # Retaliação
     p inimigo
     if inimigo.vida > 0
       inimigo_golpe = rand( inimigo.forca + inimigo.arma )
       puts "[Seu inimigo golpeia com #{ inimigo_golpe } pontos de dano!]"
       self.golpear( inimigo_golpe )
     end
   end

 end

 class ArrayDeDwemthy < Array
   alias _inspect inspect
   def inspect; "#<#{ self.class }#{ _inspect }>"; end
   def method_missing( meth, *args )
     resposta = first.send( meth, *args )
     if first.vida <= 0
       shift
       if empty?
         puts "[Uauu.  Você dizimou o Array de Dwemthy!]"
       else
         puts "[Prepare-se. #{ first.class } surgiu.]"
       end
     end
     resposta || 0
   end
 end

# Acrescentamos dois métodos em criatura - golpear, que leva um ataque, e lutar, que
# ataca um oponente (mas cuidado, ele pode revidar!)
# Antes de explcar o ArrayDeDwemthy, vamos conhecer nosso herói!
# Salve esse código como coelho.rb

class Coelho < Criatura
   caracteristicas :bombas

   vida 10
   forca 2
   carisma 44
   arma 4
   bombas 3

   # bumeranguinho
   def ^( inimigo )
     lutar( inimigo, 13 )
   end
   # a espada do herói é ilimitada!!
   def /( inimigo )
     lutar( inimigo, rand( 4 + ( ( inimigo.vida % 10 ) ** 2 ) ) )
   end
   # alface irá formar a sua força e fibra alimentar extra
   # voará na cara de seu oponente!!
   def %( inimigo )
     alface = rand( carisma )
     puts "[Alface saudável lhe dá #{ alface } pontos de vida!!]"
     @vida += alface
     lutar( inimigo, 0 )
   end
   # bombas, mas você possui somente três!!
   def *( inimigo )
     if @bombas.zero?
       puts "[HUMM!! Você está sem bombas!!]"
       return
     end
     @bombas -= 1
     lutar( inimigo, 86 )
   end
 end

# 4 armas. Bumerangue. Espada. Alface. Bombas.
# Abra o irb. Vamos ver como você está.

irb> require 'dwemthy'
irb> require 'coelho'

# Agora, mostre-se.

irb> c = Coelho.new
irb> c.vida
=> 10
irb> c.forca
=> 2

# Antes de ir para o ArrayDeDwenthy, vamos ver como você se sai
# conta uma ScubaArgentina

>> class ScubaArgentina < Criatura
>>  vida 46
>>  forca 35
>>  carisma 91
>>  arma 2
>> end
>> c = Coelho.new
>> s = ScubaArgentina.new
# Agora ataque com o bumerangue
>> c ^ s
=> [Você golpeia com 2 pontos de dano!]
=> #<ScubaArgentina:0x808c864 @carisma=91, @forca=35, @vida=44, @arma=2>
=> [Seu inimigo golpeia com 28 pontos de dano!]
=> [O Coelho morreu.]

# Finge que você não morreu e cria um novo coelho
irb> r = Coelho.new
# atacando com bumerangue!
irb> r ^ s
# os golpes de espada do herói!
irb> r / s
# comendo alface lhe dá vida!
irb> r % s
# você possui três bombas!
irb> r * s

# *, /, ^ e % são geralmente operadores matemáticos
# mas não passam de métodos que você pode reescrever
# Agora vamos pro Array de Dwemthy
# Seus inimigos: (salve como monstros.rb)
 class MacacoIndustrialEntusiasmante < Criatura
   vida 46
   forca 35
   carisma 91
   arma 2
 end

 class AnjoDosAnoes < Criatura
   vida 540
   forca 6
   carisma 144
   arma 50
 end

 class TentaculoViceAssistenteEOmbudsman < Criatura
   vida 320
   forca 6
   carisma 144
   arma 50
 end

 class CervoDeDentes < Criatura
   vida 655
   forca 192
   carisma 19
   arma 109
 end

 class IntrepidoCiclistaDecomposto < Criatura
   vida 901
   forca 560
   carisma 422
   arma 105
 end

 class Dragao < Criatura
   vida 1340     # escamas duras
   forca 451     # veias ressaltadas
   carisma 1020  # sorriso dentado
   arma 939      # cospe fogo
  end

>> require 'monstros'

>> dwary = ArrayDeDwemty[MacacoIndustrialEntusiasmante.new,
>>                     AnjoDosAnoes.new,
>>                     TentaculoViceAssistenteEOmbudsman.new,
>>                     CervoDeDentes.new,
>>                     IntrepidoCiclistaDecomposto.new,
>>                     Dragao.new]

# Você vai lutar com os monstros do Array um a um
>> r % dwary

 class ArrayDeDwemthy < Array # herda de array, então se comporta como um. e cheio de monstros.
   alias _inspect inspect
   def inspect; "#<#{ self.class }#{ _inspect }>"; end
   def method_missing( meth, *args )
     resposta = first.send( meth, *args ) # se o array recebe uma mensagem que não entende, passa ela pro primeiro monstro
     if first.vida <= 0
       shift                              # tira o primeiro monstro quando ele morre
       if empty?
         puts "[Uauu.  Você dizimou o Array de Dwemthy!]"
       else
         puts "[Prepare-se. #{ first.class } surgiu.]"
       end
     end
     resposta || 0
   end
 end

# inspect? o que é inspect?
irb> o = Object.new
=> #<Object:0x81d60c0>
irb> o.inspect
=> "#<Object:0x81d60c0>"

irb> class Coelho
irb>   attr_accessor :slogan
irb>   def initialize s; @slogan = s; end
irb>   def inspect; "#<#{ self.class } diz '#{ @slogan }'>"; end
irb> end
irb> Coelho.new "eu arrebentei a cara do dragão!!"
=> #<Coelho diz 'eu arrebentei a cara do dragão!!'>
# cada vez que você executa algum código no irb, o valor de retorno daquele código é inspecionado
# quer fazer seu próprio irb?
loop do
  print ">> "
  puts  "=> " + eval( gets ).inspect
end

# é isso!


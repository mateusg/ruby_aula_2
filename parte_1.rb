# baseado em http://www.akitaonrails.com/2008/11/10/micro-tutorial-de-ruby-parte-i

## O que tem de diferente no Ruby? ##

# Recapitulando...

class Pessoa
  def initialize(nome) # construtor, chamado quando um objeto é instanciado
    @nome = nome # @ são variáveis de instância
  end

  def nome
    @nome
  end
  # também podia ser escrito como
  # def nome; @nome; end

  def nome=(valor)
    @nome = valor
  end
  # também podia ser
end

fabio = Pessoa.new("Fabio")  # instanciando uma classe
puts fabio.nome # System.out.println

# Em java:
# Não é muito diferente

class Pessoa {
  String nome;
  public Pessoa(String nome) {
    this.nome = nome;
  }
  public String getNome() {
    return this.nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
}

Pessoa fabio = new Pessoa("Fabio");
System.out.println(fabio.getNome());

# Reescrevendo a classe

require 'ostruct' # OpenStruct, classe padrão do Ruby
                  # permite que os objetos instanciados a partir dela
                  # tenham qualquer atributo que for necessário em tempo de execução 
class Pessoa < OpenStruct; end # herança

fabio = Pessoa.new :nome => "Fabio" # símbolos são como strings. só que... especiais.
puts fabio.nome

fabio.email = 'akitaonrails@mac.com'
puts fabio.email

# e no irb
# >> é o que você digita
# => é o resultado do comando
>> 1 + 2
=> 3
>> numero = 5
=> 5
>> numero * 3
=> 15

>> contador = 1
=> 1
>> while contador < numero
>>   puts contador
>>   contador += 1
>> end
1
2
3
4

>> def hello
>>   "Hello World"
>> end

>> puts hello
Hello World

## Quase tudo é objeto ##
# influenciada por smalltalk

# hello world em java
class Teste {
  public static void main(String[] args) {
    System.out.println("Hello World");
  }
}

# hello world em ruby
puts "Hello World"

# entendendo...
>> puts "Hello World"
Hello World
=> nil

>> self # já estamos dentro de um objeto!
=> main
>> self.class # criar um método no irb é adicioná-lo a classe Object
=> Object

# mais coisa legal
# todos esse são objetos que respondem a métodos - até nil
>> 1.class
=> Fixnum

>> true.class
=> TrueClass

>> false.class
=> FalseClass

>> nil.class
=> NilClass

## Classes abertas ##

# interfaces fechadas, classes fechadas, mudar de idéia é difícil
# se precisarmos de mais funcionalidade:

class StringUtils {
  public static boolean isEmpty(String str) {
    return str == null || str.length() == 0;
  }
}

String nome = "Fabio Akita";
StringUtils.isEmpty(nome); # procedural, não orientado a objeto

# em ruby:
# podemos abrir uma classe e adicionar funcionalidades! todas as instâncias de String,
# novas e que já existem, vão ter esse método
class String
  def empty? # ? no final do método pode. por convenção, retornal true ou false
    self.nil? || self.size == 0
  end
end

nome = "Fabio Akita"
nome.empty?

# e cadê objeto aqui?
>> 1 + 2
=> 3
# também pode ser escrito como:
>> 1.+(2)
=> 3
# somar dois métodos = chamar o método + no 1o passando o 2o como parâmetro
# vale pra /, *, -, etc.
# se eu passar uma string
>> 1 + "2"
TypeError: String can't be coerced into Fixnum
  from (irb):35:in `+'
  from (irb):35
  
>> 1.+("s")
TypeError: String can't be coerced into Fixnum
  from (irb):36:in `+'
  from (irb):36
# ban! erro! 
# mas e se eu quiser que 1 + "2" seja "12"? 

# passo 1

class Fixnum # reabrindo fixnum
   alias :soma_velha :+ # alias cria um outro 'apelido' para um método
                        # agora soma_velha e + apontam pro mesmo método
end
# quando a gente abre uma classe, os métodos dentro dela são executados
# tudo a mesma coisa:
>> 5 + 10
=> 15

>> 5.+(10)
=> 15

>> 5.soma_velha(10)
=> 15

# passo 2

class Fixnum
   def +(valor)
     return self.to_s + valor if valor.is_a? String # parênteses são opcionais
     soma_velha(valor)
   end
end

# e agora:

>> 1 + 2
=> 3
>> 1 + "2"
=> "12"

# isso é Monkey Patching
# use com moderação

# mais exemplos de monkey patching

# carrega o pacote activesupport
>> require 'rubygems'
>> require 'activesupport'
=> true

# horário atual
>> Time.now
=> Wed Oct 29 23:36:24 -0200 2008

# fazendo cálculos com datas
>> Time.now - 23.days
=> Mon Oct 06 23:36:28 -0300 2008

>> 2.weeks.ago
=> Wed Oct 15 23:36:32 -0200 2008

# fazendo cálculos com unidades de medida
>> (1.gigabyte - 512.megabytes) / 1.kilobyte
=> 524288

# transformando objetos em XML
>> { :html => { :body => { :p => "teste" } } }.to_xml
=> "<?xml version="1.0" encoding="UTF-8"?>\n
<hash>\n  <html>\n    <body>\n      
<p>teste</p>\n    </body>\n  </html>\n
</hash>\n"

# em java, não podemos

String a = "foo";
String b = "bla";
if (a == b) {
  System.out.println("encontrado!");
}
if (a > b) {
  System.out.println("a maior do que b");
}

# temos que

if (a.equals(b)) {
  System.out.println("encontrado!");
}
if (a.compareTo(b) > 0) {
  System.out.println("a maior do que b");
}

# em ruby

a = "foo"
b = "bla"
puts "encontrado!" if a == b
puts "a maior do que b" if a > b # ==, >, >=, são todos simples métodos

## Módulos e Organização ##

# podemos fazer diferente

Fixnum.class_eval do
  alias :soma_velha :+
  def +(valor)
    return self.to_s + valor if valor.is_a? String
    soma_velha(valor)
  end
end
# faz a mesma coisa que o anterior
# classes são objetos (da classe Class), então podemos chamar métodos neles
# a diferença é que assim podemos colocar no método. saca:

def fazGritar(quem)
  quem.instance_eval do
    def grita
      puts 'AAAHHHHH'
    end
  end
end

fazGritar(String)
"Olá".grita
fazGritar(Fixnum)
3.grita

# podemos ser mais criteriosos:
>> a = "teste"
=> "teste"
>> a.instance_eval do # abrimos uma instância!
?>   def hello
>>     "hello from teste"
>>   end
>> end
=> nil
>> a.hello
=> "hello from teste"
>> "foo".hello # só aquela instância tem esse método
NoMethodError: undefined method `hello` for "foo":String
  from (irb):17
  from :0

# outro jeito de abrir classes:
module MeusPatches # definindo módulos
  def say_hello
    "Hello World!"
  end

  def say_time
    Time.now
  end
end

class Fixnum
  include MeusPatches
end

class String
  extend MeusPatches
end

# módulos são como classes que não podem ser instanciadas. são bibliotecas de código.

>> 13.say_hello
=> "Hello World!"

>> "".say_hello
NoMethodError: undefined method `say_hello` for "":String
  from (irb):19

>> String.say_hello
=> "Hello World!"

# include acrescenta métodos *aos objetos*. extend adiciona métodos *à classe*
# módulos são muito usados para organizar código
# ActiveRecord é um framework de mapeamento objeto/relacional. tem muita funcionalidade
# e cada uma fica num módulo/arquivo separado
ActiveRecord::Base.class_eval do
  extend ActiveRecord::QueryCache
  include ActiveRecord::Validations
  include ActiveRecord::Locking::Optimistic
  include ActiveRecord::Locking::Pessimistic
  include ActiveRecord::AttributeMethods
  include ActiveRecord::Dirty
  include ActiveRecord::Callbacks
  include ActiveRecord::Observing
  include ActiveRecord::Timestamp
  include ActiveRecord::Associations
  include ActiveRecord::NamedScope
  include ActiveRecord::AssociationPreload
  include ActiveRecord::Aggregations
  include ActiveRecord::Transactions
  include ActiveRecord::Reflection
  include ActiveRecord::Calculations
  include ActiveRecord::Serialization
end

# módulos tem eventos
# e podem ser aninhados

module MeusPatches
  def self.included(base)
    base.send(:extend, ClassMethods)
    puts "Module MeusPaches incluso na classe #{base.name}"
  end

  def metodo_de_instancia
    "sou um metodo de instancia"
  end

  module ClassMethods
    def self.extended(base)
      puts "Module MeusPatches::ClassMethods extendido na classe #{base.name}"
    end

    def metodo_de_classe
      "sou um metodo de classe"
    end
  end
end

class Pessoa
end

?> Pessoa.send(:include, MeusPatches) # calma, você vai entender já já
Module MeusPatches::ClassMethods extendido na classe Pessoa
Module MeusPaches incluso na classe Pessoa
=> Pessoa
>> 
?> fabio = Pessoa.new
=> #<Pessoa:0x1789528>
>> fabio.metodo_de_instancia
=> "sou um metodo de instancia"
>> 
?> Pessoa.metodo_de_classe
=> "sou um metodo de classe"

# sem módulos, equivale a 
class Pessoa
  def metodo_de_instancia
    "sou um metodo de instancia"
  end

  def self.metodo_de_classe    # self é 'disponível pra classe'. mas não é estático, 
    "sou um metodo de classe"  # poque tudo em ruby é dinâmico
  end
end

# outra forma: 

class Pessoa
  def metodo_de_instancia
    "sou um metodo de instancia"
  end

  class << self            # reabrindo a metaclasse
    def metodo_de_classe   # é como se tudo que estiver aqui dentro tivesse um 'self' antes
      "sou um metodo de classe"
    end
  end
end

>> p = Pessoa.new
=> #<Pessoa:0x1775370>

>> p.metodo_de_instancia
=> "sou um metodo de instancia"

>> p.metodo_de_classe
NoMethodError: undefined method `metodo_de_classe` for #<Pessoa:0x1775370>
  from (irb):94

>> Pessoa.metodo_de_instancia
NoMethodError: undefined method `metodo_de_instancia` for Pessoa:Class
  from (irb):95

>> Pessoa.metodo_de_classe
=> "sou um metodo de classe"
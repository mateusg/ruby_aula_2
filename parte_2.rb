# baseado em http://www.akitaonrails.com/2008/11/10/micro-tutorial-de-ruby-parte-ii

## Métodos vs Mensagens ##
# nós não 'chamamos métodos do objeto'
# nós 'enviamos mensagens ao objeto'

>> "teste".hello
NoMethodError: undefined method `hello` for "teste":String
  from (irb):1
  
# “tentamos chamar o método ‘hello’ que não existe em String.” -> ERRADO
# “tentamos enviar a mensagem ‘hello’ ao objeto e sua resposta padrão é que ele não sabe responder a essa mensagem.” -> CERTO

>> "teste".send(:hello)
NoMethodError: undefined method 'hello' for "teste":String
  from (irb):22:in 'send'
  from (irb):22
  
>> "teste".diga("Fabio")
NoMethodError: undefined method 'diga' for "teste":String
  from (irb):24

>> "teste".send(:diga, "Fabio")
NoMethodError: undefined method 'diga' for "teste":String
  from (irb):25:in  'send'
  from (irb):25

# nada é fechado, envie a mensagem que você quiser
# existem duas formar de resolver esse problema:
# 1) Abrir a classe String e adicionar o método hello ou dig
# 2) Fazer String receber qualquer mensagem, independente se existe um método ou não

class String
  def method_missing(metodo, *args)
    puts "Nao conheco o metodo #{metodo}. Os argumentos foram:"
    args.each { |arg| puts arg }
  end
end

>> "teste".hello
Nao conheco o metodo hello. Os argumentos foram:
=> []

>> "teste".diga("Fabio")
Nao conheco o metodo diga. Os argumentos foram:
Fabio
=> ["Fabio"]

>> "teste".blabla(1,2,3)
Nao conheco o metodo blabla. Os argumentos foram:
1
2
3

# não estamos chamando métodos que não existem, estamos enviando mensagens =D
class Teste
  private
  def alo
     "alo"
  end
end

>> Teste.new.alo
NoMethodError: private method 'alo' called for #<Teste:0x17d3b3c>
  from (irb):47

>> Teste.new.send(:alo)
=> "alo"

# mostrando melhor
gem install builder

>> require 'rubygems'
>> require 'builder'
=> false
>> x = Builder::XmlMarkup.new(:target => $stdout, :indent => 1)
<inspect/>
=> #<IO:0x12b7cc>
>> x.instruct!
<?xml version="1.0" encoding="UTF-8"?>
=> #<IO:0x12b7cc>
>> x.pessoa do |p|
?>     p.nome "Fabio Akita"
>>     p.email "fabioakita@gmail.com"
>>     p.telefones do |t|
?>       t.casa "6666-8888"
>>       t.trabalho "2222-3333" # vamos enviando mensagens e ele vai criando o xml
>>     end                      # legal, não?
>>   end
<pessoa>
 <nome>Fabio Akita</nome>
 <email>fabioakita@gmail.com</email>
 <telefones>
  <casa>6666-8888</casa>
  <trabalho>2222-3333</trabalho>
 </telefones>
</pessoa>
=> #<IO:0x12b7cc>

# olha o jeito tradicional:

import org.w3c.dom.*;
import javax.xml.parsers.*; 
import javax.xml.transform.*; 

class CreateDomXml 
{
  public static void main(String[] args) 
  {
    try{
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = factory.newDocumentBuilder();
      Document doc = docBuilder.newDocument();
      
      Element root = doc.createElement("pessoa");
      doc.appendChild(root);
      
      Element childElement = doc.createElement("nome");
      childElement.setValue("Fabio Akita");
      root.appendChild(childElement);
      
      childElement = doc.createElement("email");
      childElement.setValue("fabioakita@gmail.com");
      root.appendChild(childElement);
      .....
    } catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }
}

## High order functions e Blocos ##

# digamos que queremos ter uma 'Botão' que, quando clicado, faz alguma coisa que
# a gente tenha configurado... humm...
# em java:

interface Command {
  public void execute();
}

class Button {
  Command action;
  public void setCommand(Command action) {
    this.action = action;
  }
  public void click() {
    this.action.execute();  
  }
}

Button myButton = new Button();
myButton.setCommand(new Command() {
  public void execute() {
    System.out.println("Clicked!!");
  }
});

# em ruby:

class Button
  def initialize(&block)
    @block = block
  end
  def click
    @block.call
  end
end

my_button = Button.new { puts "Clicked!!" }
my_button.click
# não precisamos encapsular uma função como um método dentro de uma classe
# funções são cidadãos de primeira classe
# o código dentro das chaves é encapsulado diretamente num objeto Proc (método)

# exemplo mais simples:

>> def diga_algo(nome)
>>   yield(nome) # executa o bloco recebido
>> end
=> nil

>> diga_algo("Fabio") { |nome| puts "Olá, #{nome}" } # um bloco pode receber parâmetros
Hello, Fabio
=> nil

>> diga_algo("Akita") { |nome| puts "Hello, #{nome}" }
Hello, Akita
=> nil

# prestenção:

{ |nome| puts "Hello, #{nome}" }

# é uma função anônima. dentro dos || são parâmetros
# o yield vai executar esse bloco, e pode passar parâmetros pra ele

# outro exemplo:

>> soma = lambda { |a, b| a + b }
=> #<Proc:0x012d4898@(irb):46>
>> soma.call(1,2)
=> 3
>> soma.call(4,4)
=> 8

# lambda captura um bloco e guarda num objeto Proc

# blocos podem ser passados como parâmetros, guardados em variáveis
# e retornados em funções

>> def criar_bloco(nome)
>>   lambda { puts "Hello #{nome}"}
>> end
=> nil
>> 
?> fabio = criar_bloco("Fabio")
=> #<Proc:0x0124aae4@(irb):59>
>> akita = criar_bloco("Akita")
=> #<Proc:0x0124aae4@(irb):59>
>> fabio.call
Hello Fabio
=> nil
>> akita.call
Hello Akita
=> nil
>> fabio.call
Hello Fabio
=> nil

# se quisermos ler um arquivo...

# em java: 

StringBuilder contents = new StringBuilder();
    
try {
  BufferedReader input =  new BufferedReader(
    new FileReader(aFile));
  try {
    String line = null; 
    while (( line = input.readLine()) != null){
      contents.append(line);
      contents.append(
        System.getProperty("line.separator"));
    }
  }
  finally {
    input.close();
  }
}
catch (IOException ex){
  ex.printStackTrace();
}

# em ruby:

contents = ""
begin
  input = File.open(aFile, "r")
  begin
    while line = input.gets
      contents << line
      contents << "\n"
    end
  ensure
    input.close
  end
rescue e
  puts e
end

# nada mudou muito. mas a gente só quer *abrir* o arquivo, o código aí abre, 
# garante que vai ser fechado e trata exceções
# ruby tem um open mais puro:
contents = ""
File.open(aFile, "r") do |input|
  while line = input.gets
    contents << line
    contents << "\n"
  end
end

# se quisermos implementá-lo
class File 
  def File.open(*args) 
    result = f = File.new(*args) 
    if block_given? 
      begin 
        result = yield f 
      ensure 
        f.close 
      end 
    end 
    return result 
  end 
end 

## Iteradores ##

contents = File.open(aFile).readlines.inject("") do |buf, line| 
  buf += line
end

# readlines retorna um array com as linhas do arquivo
# inject é um redutor. pega um array e transforma num elemento
# não pense em como vai iterar elemento por elemento
# pense só no que vai fazer eles
>> [1,2,3,4,5].map { |elem| elem * elem }
=> [1, 4, 9, 16, 25]

>> [1,2,3,4,5].select { |elem| elem % 2 == 0 }
=> [2, 4]

>> [1,2,3,4,5].inject(0) { |total, elem| total += elem }
=> 15

>> total = 0
=> 0
>> [1,2,3,4,5].each { |elem| total += elem }
=> [1, 2, 3, 4, 5]
>> total
=> 15

# duas sintaxes
[1,2,3,4].each { |elem| puts elem }

[1,2,3,4].each do |elem|
  puts elem
end

## Tipos Básicos ##

# array
>> lista = [100,200,300,400]
=> [100, 200, 300, 400]
>> lista[2]
=> 300
>> lista.first
=> 100
>> lista.last
=> 400

>> [1,2,3,4] + [5,6,7,8]
=> [1, 2, 3, 4, 5, 6, 7, 8]

>> [1,2,3,4,5] - [2,3,4]
=> [1, 5]

>> [1,2,3] * 2
=> [1, 2, 3, 1, 2, 3]

# [] é um método

class Array
  alias :seletor_antigo :[]
  def [](indice)
    return seletor_antigo(indice) if indice.is_a? Fixnum
    return self.send(indice) if indice.is_a? Symbol
    "Nada encontrado para #{indice}"
  end
end

>> lista = [1,2,3,4]
=> [1, 2, 3, 4]

>> lista[2]
=> 3

>> lista[:first]
=> 1

>> lista[:last]
=> 4

>> lista[:size]
=> 4

>> lista["bla bla"]
=> "Nada encontrado para bla bla"

# Hashes
# é um dicionário. cada valor é indexado por uma chave.

>> dic = { "car" => "carro", "table" => "mesa", "mouse" => "rato" }
=> {"mouse"=>"rato", "table"=>"mesa", "car"=>"carro"}

>> dic["car"]
=> "carro"

>> dic["mouse"]
=> "rato"

>> dic.merge!( {"book" => "livro", "leg" => "perna"} ) # com ! > muda valor, destrutivo
=> {"leg"=>"perna", "mouse"=>"rato", "table"=>"mesa", "book"=>"livro", "car"=>"carro"}

>> dic = dic.merge( {"book" => "livro", "leg" => "perna"} ) # sem ! > retorna, não muda

>> fabio = { "emails" => 
>>           { "trabalho" => "fabio.akita@locaweb.com.br",
>>              "normal"   => "fabioakita@gmail.com" } }
=> {"emails"=>{"normal"=>"fabioakita@gmail.com", "trabalho"=>"fabio.akita@locaweb.com.br"}}

>> fabio["emails"]["normal"]
=> "fabioakita@gmail.com"

>> dic.keys
=> ["leg", "mouse", "table", "book", "car"]
>> dic.values
=> ["perna", "rato", "mesa", "livro", "carro"]

>> dic.keys.each do |chave|
>>   puts "#{chave} = #{dic[chave]}"
>> end
leg = perna
mouse = rato
table = mesa
book = livro
car = carro

# Strings

>> palavra = "bla bla"
=> "bla bla"
>> palavra.size
=> 7
>> nome = "Fabio"
=> "Fabio"
>> sobrenome = "Akita"
=> "Akita"
>> nome + " " + sobrenome
=> "Fabio Akita"

>> "#{nome} #{sobrenome}" # interpolação - tudo dentro de #{} é executado e
=> "Fabio Akita"          # convertido em string

# strings com quebra de linha:

>> nome = "F"
=> "F"
>> query = "SELECT * FROM NOMES \n" +
?>     "WHERE NOME LIKE '%" + nome + "%' \n" +
?>     "ORDER BY NOME"
=> "SELECT * FROM NOMES \nWHERE NOME LIKE '%F%' \nORDER BY NOME"

# reescrevendo:

>> nome = "F"
=> "F"      # esse STR pode ser qualquer coisa
>> query = <<-STR
  SELECT * FROM NOMES
  WHERE NOME LIKE '%#{nome}%'
  ORDER BY NOME
STR
=> "  SELECT * FROM NOMES\n  WHERE NOME LIKE '%F%'\n  ORDER BY NOME\n"

# Símbolos

dic1 = { "leg" => "perna" }
dic2 = { :leg => "perna" } # símbolos são mais legíveis e ocupam menos memória

>> a = :leg # símbolos são imutáveis
=> :leg
>> b = :leg
=> :leg
>> a.object_id # mesmo conteúdo, mesmo objeto
=> 531778
>> b.object_id
=> 531778

>> a = "leg"
=> "leg"
>> b = "leg"
=> "leg"
>> a.object_id # mesmo conteúdo, objetos diferentes
=> 11823830
>> b.object_id
=> 11820130

# existem outros imutáveis em ruby
>> a = 1
=> 1
>> b = 1
=> 1
>> c = true
=> true
>> d = true
=> true

>> a.object_id == b.object_id
=> true
>> c.object_id == d.object_id
=> true








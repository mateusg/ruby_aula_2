## Métodos ##

# em algumas linguagens temos overloading
public class Pessoa {
  String nome;
  String email;

  public Pessoa(String nome, String email) {
    this.nome = nome;
    this.email = email;
  }

  public Pessoa(String nome) {   # mesmo nome e parâmetros diferentes = métodos diferentes
    this(nome, "");
  }
}

Pessoa fabio = new Pessoa("Fabio", "fabio@foo.com.br");
Pessoa akita = new Pessoa("Akita");

# em ruby temos várias maneiras de lidar com isso.
# veja:

class Pessoa
  def initialize(nome, email = "") # valor padrão, caso a gente não passe nada
    @nome, @email = nome, email    # atribuição em massa
  end
end

class Pessoa
  def initialize(nome, *args) # * (splat) é um buraco-negro -> pega todos os parâmetros
    @nome = nome              # e coloca num array
    @args = args              # assim temos métodos com um número infinito de parâmetros
  end
  def args
    @args
  end
end

>> fabio = Pessoa.new("Fabio", "fabio@foo.com.br", "6666-6666")
=> #<Pessoa:0x26ee61c @args=["fabio@foo.com.br", "6666-6666"], @nome="Fabio">
>> fabio.args.first
=> "fabio@foo.com.br"
>> fabio.args.last
=> "6666-6666"

# outro exemplo

def soma(*args)
  args.inject(0) { |elem, total| total += elem }
end

>> soma(2,2)
=> 4
>> soma(1,2,3,4)
=> 10
>> soma(100,200,300)
=> 600

# posso passar um array?

>> items = [1,2,3,4,5,6]
>> soma items
TypeError: can''t convert Fixnum into Array
  from (irb):31:in '+'       # args não é um array com os elementos 1, 2, 3, ...
  from (irb):31:in 'soma'    # args é um array onde o 1o elemento é um array
  from (irb):40:in 'inject'
  from (irb):31:in 'each'
  from (irb):31:in 'inject'
  from (irb):31:in 'soma'
  from (irb):40
  
# o que queremos é passar todos os items expandidos

>> items = [1,2,3,4,5,6]
=> [1, 2, 3, 4, 5, 6]
>> soma *items     # agora sim!
=> 21

# quando temos um método com muitos condicionais, como fica?
# exemplo: criando um sql
>> Person.find :first, :conditions => { :nome => "Fabio" }, :order => :nome
=> SELECT * FROM "people" WHERE ("people"."nome" = 'Fabio') ORDER BY nome LIMIT 1

def find(*args)
  options = args.extract_options!

  case args.first
    when :first then find_initial(options)
    when :last  then find_last(options)
    when :all   then find_every(options)
    else             find_from_ids(args, options)
  end
end

# exemplo mais simples

class Pessoa
  attr_accessor :primeiro_nome, :sobrenome, :iniciais, :email # cria getters e setters
  def initialize(primeiro_nome = "", options = {})            # pra variáveis de instância
    @primeiro_nome = primeiro_nome
    @sobrenome = options[:sobrenome]
    @iniciais = options[:iniciais]
    @email = options[:email]
  end
end

>> fabio = Pessoa.new "Fabio", :sobrenome => "Akita", :iniciais => "FMA"
#<Pessoa:0x26b2 @iniciais="FMA", @sobrenome="Akita", @email=nil, @primeiro_nome="Fabio">
>> fabio.primeiro_nome
=> "Fabio"
>> fabio.sobrenome
=> "Akita"
>> fabio.iniciais
=> "FMA"
>> fabio.email
=> nil

# esperamos dois parâmetros, uma string e um hash
# podia ser assim
fabio = Pessoa.new("Fabio", {:sobrenome => "Akita", :iniciais => "FMA"})
# mas {} são opcionais

## Métodos Dinâmicos ##

# metaprogramação é modificar em tempo de execução
# o comportamento de um objeto

class Pessoa
  attr_accessor :nome, :email
end

>> fabio = Pessoa.new
=> #<Pessoa:0x26a6f24 @iniciais=nil, @sobrenome=nil, @email=nil, @primeiro_nome="">
>> fabio.nome = "Fabio"
=> "Fabio"
>> fabio.nome
=> "Fabio"

# podemos implementar nosso próprio attr_accessor

class Object # vai estar presente em todas as classes, já que todas herdam de Object
  def my_accessor( *symbols )
    symbols.each do |symbol|
      module_eval( "def #{symbol}() @#{symbol}; end" ) # executa a string passada
      module_eval( "def #{symbol}=(val) @#{symbol} = val; end" )
    end
  end
end

class Pessoa
  my_accessor :telefone
end

>> fabio = Pessoa.new
=> #<Pessoa:0x2690f30 @iniciais=nil, @sobrenome=nil, @email=nil, @primeiro_nome="">
>> fabio.telefone = "6666-6666"
=> "6666-6666"
>> fabio.telefone
=> "6666-6666"

# mesma coisa que:

class Pessoa
  def telefone() @telefone; end
  def telefone=(val) @telefone = val; end
end

# reflexão é descobrir coisas sobre um objeto (que mensagens ele responde)

>> a = "string"
=> "string"
>> a.methods
=> ["chop!", "constantize", ... "squish", "<<"]
>> a.respond_to? :size

>> p = Pessoa.new
=> #<Pessoa:0x261af60 @iniciais=nil, @sobrenome=nil, @email=nil, @primeiro_nome="">
>> p.telefone
=> nil
>> p.instance_variable_set("@telefone", "3333-4444")
=> "3333-4444"
>> p.instance_variable_get("@telefone")
=> "3333-4444"

## Nulo ##
# nulo em ruby é um objeto nil

>> nil
=> nil
>> nil.class
=> NilClass
>> nil.object_id
=> 4

# se quisermos fazer alguma coisa em um objeto só se ele não for nulo
unless obj.nil? # unless == if not, só que mais legível
  obj.say_hello # qualquer objeto responde a nil?
else
  nil
end

>> nil.nil?
=> true
>> 1.nil?
=> false

obj ? obj.say_hello : nil # operador ternário. só nil e false respondem como false
                          # em condicionais

# Considerações finais: essa é só a ponta do iceberg
# corra pra apidock.com quando tiver uma dúvida
# não escreva ruby como você escreve outra coisa! ruby é ruby! =D
# quiz bem legais em http://rubyquiz.com/